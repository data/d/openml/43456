# OpenML dataset: IMDb-Indonesian-Movies

https://www.openml.org/d/43456

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This IMDb Indonesian Movies Dataset contains information of 1262 Indonesian movies. The data was gathered using IMDb-Scraper and then was converted and cleaned into a .csv file.
Acknowledgements
This dataset is collected from IMDb.com by using IMDb-Scraper by dojutsu-user.
Content
There are more than 1200+ Indonesian Movies in the dataset consisting of 11 columns relating to each movie. Those columns are:

title
year
description
genre
rating
users_rating
votes
languages
directors
actors
runtime

Banner image source: Whatthefan!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43456) of an [OpenML dataset](https://www.openml.org/d/43456). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43456/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43456/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43456/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

